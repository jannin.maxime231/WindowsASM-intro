; msgbox.asm
	
.386
.model flat, stdcall
option casemap:none

      include \masm32\include\windows.inc
      include \masm32\include\user32.inc
      include \masm32\include\kernel32.inc

      includelib \masm32\lib\user32.lib
      includelib \masm32\lib\kernel32.lib

.data
	msgbox2path	db	"msgbox2.exe",0
	dll db  "user32.dll"
	msgBoxFunc db  "MessageBoxA"
	
.code

start:
    push NULL
    push FILE_ATTRIBUTE_NORMAL
    push OPEN_EXISTING
    push NULL
    push NULL
    push GENERIC_READ+GENERIC_WRITE
    push offset msgbox2path
    call CreateFile    
    cmp eax, INVALID_HANDLE_VALUE
    je _end

    push NULL
    push NULL
    push NULL
    push PAGE_READWRITE
    push NULL
    push eax
    call CreateFileMapping

    push NULL
    push NULL
    push NULL
    push FILE_MAP_ALL_ACCESS
    push eax
    call MapViewOfFile

	; copy my dllname to msgbox2 .data section
	cld
	lea esi, [offset dll]
	lea edi, [eax + 820h]
	mov ecx, 10
	rep movsb
	
	; copy my msgBoxFunc name in msgbox2 .data section
	cld
	lea esi, [offset msgBoxFunc]
	lea edi, [eax + 830h]
	mov ecx, 11
	rep movsb
	
	; change jmp offset (to jmp to the injected code)
    mov BYTE PTR [eax + 401h], 26h
	
	; get addr of LoadLib : get ExitProcess addr 
	; then add the offset (between ExitProcess and LoadLibraryA) to get LoadLibraryA addr
	; addr is put in eax
	mov BYTE PTR [eax + 428h], 0A1h
	mov DWORD PTR [eax + 429h], 00402000h
	mov BYTE PTR [eax + 42Dh], 05h
	mov DWORD PTR [eax + 42Eh], 00001DC0h
	
	; push dllname (in data section)
	mov BYTE PTR [eax + 432h], 68h
	mov DWORD PTR [eax + 433h], 00403020h

	; call to LoadLibraryA (in eax)
	mov WORD PTR [eax + 437h], 0D0FFh

	; put eax value (loadlibrary return) into ebx (to save it)
	; xchg ebx, eax
	mov BYTE PTR [eax + 439h], 93h
	
	; get addr of GetProcAddr (same than LoadLibraryA, with different offset)
	; put the addr in eax
	mov BYTE PTR [eax + 43Ah], 0A1h
	mov DWORD PTR [eax + 43Bh], 00402000h
	mov BYTE PTR [eax + 43Fh], 05h
	mov DWORD PTR [eax + 440h], 000014F0h
		
	; push args for GetProcAddr
	; string in data section corresponding to the funcName (MessageBoxA)
	mov BYTE PTR [eax + 444h], 68h
	mov DWORD PTR [eax + 445h], 00403030h
	; push ebx (return of loadlibrary previously in eax)
	mov BYTE PTR [eax + 449h], 53h
	
	; call to GetProcAddress (via eax)
	mov WORD PTR [eax + 44Ah], 0D0FFh
	
	; push message box args
	; push 0
    mov BYTE PTR [eax + 44Ch], 6Ah
    mov BYTE PTR [eax + 44Dh], 0h
	; first string of .data section (title)
    mov BYTE PTR [eax + 44Eh], 68h
	mov DWORD PTR [eax + 44Fh], 00403000h
	; second string from data section (message)
	mov BYTE PTR [eax + 453h], 68h
	mov DWORD PTR [eax + 454h], 0040300Ah
    ; push 0
    mov BYTE PTR [eax + 458h], 6Ah
    mov BYTE PTR [eax + 459h], 0H
	
	; call eax (return from getprocaddress)
	mov WORD PTR [eax + 45Ah], 0D0FFh
	
	; jmp to the initial code (arg of exit process)
	mov BYTE PTR [eax + 45Ch], 0EBh
    mov BYTE PTR [eax + 45Dh], 0A4h
	
    push eax
    call UnmapViewOfFile

_end:	
	push	0
	call	ExitProcess



end	start